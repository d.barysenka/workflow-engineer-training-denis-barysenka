# **Name:** Denis Barysenka
* My profession is a civil engineer.
* I am remotely completing my master's degree in China.
* I want to become a professional programmer.
* Strong technical background and analytical skills
* Languages: English – B1, Chinese – B1
## **Skills:**
* Programming Languages: Python, HTML (basic knowledge)
* Databases: SQL (MySQL, PostgreSQL)
* OS: Windows, Linux (basic knowledge), Android, iOS
* Tools: Django 

Tools | Level
------------ | -------------
python | 4
mysql | 4
yaml | 2
csv | 2
md | 4
git | 4
redmine | 1
jira | 1
## Working hours:

MONDAY | TUESDAY | WEDNESDAY | THURSDAY | FRIDAY | SATURDAY | SUNDAY
------------ | ------------- | ------------- | ------------- | ------------- |------------- | ------------- 
12:00-20:00 | 12:00-20:00 | 12:00-20:00 | 12:00-20:00 | 12:00-20:00 | **12:00-18:00** | **12:00-18:00** |
## Links:
* **[GitLab - Denis Barysenka](https://gitlab.com/d.barysenka)**
* **[GitHub - Denis Barysenka](https://github.com/dbarysenka)**
* **[Jira - Denis Barysenka](https://workflow-engineers-syberry.atlassian.net/jira/people/604c9ec72b3f9a006a6ea6da)**
* **[Redmine - Denis Barysenka](https://www.redmine.org/users/512403)**
* **[Discord - Denis Barysenka](https://discord.gg/avQw9D4HkW)**

